package Profiles;

import Activities.baseActivity;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

//@XmlType(propOrder = { "activity", "date", "length"}, name = "acrec")
@XmlRootElement(name = "accRec")
public class ActivityRecord {
    @XmlElement
    private baseActivity activity;
    @XmlAttribute
    private Date date;
    @XmlAttribute
    private float length;

    public ActivityRecord() {
        activity = null;
        date = new Date();
        length = 0;
    }

    public ActivityRecord(baseActivity activity, Date date, float lenght) {
        this.activity = activity;
        this.date = date;
        this.length = lenght;
    }

    baseActivity getActivity() {
        return activity;
    }

    void setActivity(baseActivity activity) {
        this.activity = activity;
    }

    Date getDate() {
        return date;
    }

    void setDate(Date date) {
        this.date = date;
    }

    float getLength() {
        return length;
    }

    void setLength(float length) {
        this.length = length;
    }
}
