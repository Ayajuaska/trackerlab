package Profiles;

import Activities.Skipping;
import Activities.baseActivity;
import Errors.AppError;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import java.io.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@XmlType(propOrder = { "name", "activities", "totalCals"}, name = "group")
@XmlRootElement
public class Profile {
    @XmlAttribute
    private String name;
    @XmlElement(name="activity")
    private ActivityRecord[] activities;

    @XmlAttribute
    private float totalCals;

    private static Profile instance = null;
    private static Logger logger = Logger.getLogger(Profile.class.getName());

    public static Profile getInstance() {
        if (instance == null) {
            instance = new Profile();
        }
        return instance;
    }

    public String getName() {
        return this.name;
    }

    void setName(String name) {
        this.name = name;
    }

    ActivityRecord[] getActivities() {
        return this.activities;
    }

    public void addActivittRecord(baseActivity activity, Date date, int len) {
        ActivityRecord record = new ActivityRecord(activity, date, len);
        ActivityRecord[] newActivity = new ActivityRecord[activities.length + 1];
        System.arraycopy(activities, 0, newActivity, 0, activities.length);
        newActivity[activities.length] = record;
        this.activities = newActivity;
        this.totalCals += activity.cost((float) len / 3600);
    }

    public Profile() {
        activities = new ActivityRecord[0];
        totalCals = 0;
        name = "";
        Profile.instance = this;
    }

    public static Profile load(String path) {
        try {
            FileInputStream fis = new FileInputStream(path);
            Profile profile = unmarshall(fis);
            fis.close();
            return profile;
        } catch (IOException err) {
            throw new AppError(err.getMessage());
        }
    }

    public void save(String path) {
        File file = new File(path);
        try {
            FileOutputStream fos = new FileOutputStream(file.getCanonicalPath());
            this.marshall(fos);
            fos.close();
        } catch (IOException err) {
            throw new AppError(err.getMessage());
        }
    }

    static Profile unmarshall(InputStream input) throws AppError {
        try {
            JAXBContext context = JAXBContext.newInstance(Profile.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object o = unmarshaller.unmarshal(input);
            instance = (Profile)o;
            return instance;
        } catch (JAXBException exception) {
            logger.log(Level.SEVERE,
                    "marshallExample threw JAXBException", exception);
            throw new AppError(exception.getMessage());
        }
    }

    void marshall(OutputStream output) {
        try {
            JAXBContext context = JAXBContext.newInstance(Profile.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(this, output);
        } catch (JAXBException exception) {
            logger.log(Level.SEVERE, "marshallExample threw JAXBException", exception);
        }
    }

    public float getTotalCals() {
        return this.totalCals;
    }
}
