package Profiles;

import Events.ProfileEvent;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import sample.Main;
import sun.rmi.runtime.Log;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ProfileController {
    @FXML
    public TextField test;

    @FXML
    private ComboBox<String> namesList;

    @FXML
    private Button okButton;

    @FXML
    private Button createButton;

    private String[] profileNames;
    private Properties properties = Main.properties;

    public ProfileController() {
        profileNames = getProfileNames();
    }

    @FXML
    private void initialize() {
        System.out.println("Init");
        initProfiles();
    }

    private void initProfiles()
    {
        if (this.profileNames.length == 0) {
            return;
        }
        ObservableList<String> items = namesList.getItems();
        if (items.setAll(this.profileNames)) {
            System.out.println("Ok");
        } else {
            System.out.println("not Ok");
        }
        namesList.getSelectionModel().select(0);
        okButton.setDisable(false);
    }

    @FXML
    private void btnOk() {
        ProfileEvent event = new ProfileEvent(ProfileEvent.SELECTED);
        event.setProfile(namesList.getSelectionModel().getSelectedItem());
        okButton.fireEvent(event);
    }

    private String[] getProfileNames() {
        String[] names = new String[0];
        System.out.println(properties.getProperty("profiles.path"));
        File profilesDir;
        try {
            profilesDir = new File(properties.getProperty("profiles.path"));
        } catch (Exception exc) {
            return names;
        }
        if (!profilesDir.exists() || !profilesDir.isDirectory()) {
            return names;
        }
        File[] files = profilesDir.listFiles();
        if (files == null || files.length == 0) {
            return names;
        }
        return Arrays.stream(files).map(f -> f.getName().replaceAll(".xml", "")).toArray(String[]::new);
    }

    @FXML
    private void createNewProfile()
    {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("New profile");
        dialog.setHeaderText("Set profile name");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(this::addNewProfile);
    }

    private void addNewProfile(String name) {
        try {
            String path = properties.getProperty("profiles.path") + File.separator + name + ".xml";
            File newProfile = new File(path);
            boolean done = newProfile.createNewFile();
            if (!done) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Couldn't create file: " + path);
                return;
            }
            Profile profile = new Profile();
            profile.setName(name);
            profile.save(path);
        } catch (IOException err) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, err.getMessage());
            return;
        }
        if (this.profileNames == null) {
            profileNames = new String[]{name};
        } else {
            String[] tmp = new String[profileNames.length + 1];
            System.arraycopy(profileNames, 0, tmp, 0, profileNames.length);
            tmp[tmp.length - 1] = name;
            this.profileNames = tmp;
        }

        this.initProfiles();
        namesList.getSelectionModel().select(name);
    }
}
