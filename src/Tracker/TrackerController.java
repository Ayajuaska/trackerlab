package Tracker;

import Activities.PushUps;
import Activities.Skipping;
import Activities.Squat;
import Activities.baseActivity;
import Profiles.Profile;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import sample.Main;

import java.io.*;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TrackerController {
    @FXML
    public ComboBox<baseActivity> currentActivity;
    @FXML
    public Button StartStopBtn;
    @FXML
    public Label timerDisplay;
    public Label cals;

    private Timer timer = null;
    private TimerTask task = null;
    private boolean timer_is_running = false;
    private Logger logger;

    public TrackerController() {
        logger = Logger.getLogger(this.getClass().getName());
    }

    @FXML
    private void initialize() {
        currentActivity.getItems().setAll(new Skipping(), new PushUps(), new Squat());
        currentActivity.getSelectionModel().select(0);
        cals.setText(Float.toString(Profile.getInstance().getTotalCals()));
    }

    private void runTimer() {
        if (!timer_is_running) {
            timerDisplay.setText("0");
            timer = new Timer();
            task = new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() -> {
                        int c;
                        try {
                            c = Integer.parseInt(timerDisplay.getText());
                        } catch (NumberFormatException err) {
                            System.out.println(err.getMessage());
                            return;
                        }
                        c++;
                        timerDisplay.setText(Integer.toString(c));
                    });
                }
            };
            timer.schedule(task, 1000, 1000);
            timer_is_running = true;
        } else {
            task.cancel();
            timer.cancel();
            timer = null;
            Platform.runLater(() -> {
                baseActivity activity = currentActivity.getSelectionModel().getSelectedItem();
                Date date = new Date();
                int len = Integer.parseInt(timerDisplay.getText());

                Profile profile = Profile.getInstance();
                profile.addActivittRecord(activity, date, len);

                String savePath = Main.properties.getProperty("profiles.path") + File.separator + Profile.getInstance().getName() + ".xml";
                profile.save(savePath);
                cals.setText(Float.toString(profile.getTotalCals()));
                Profile.load(savePath);
            });
            timer_is_running = false;
        }
    }

    public void toggleTimer(ActionEvent actionEvent) {
        logger.log(Level.INFO, actionEvent.toString());
        runTimer();
    }
}
