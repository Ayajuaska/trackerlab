package Tracker;

import Errors.AppError;
import Windows.IWindow;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.io.InputStream;

public class TrackerWindow implements IWindow {
    private Scene scene;

    public TrackerWindow() {
        VBox box;
        FXMLLoader loader = new FXMLLoader();
        InputStream res = ClassLoader.getSystemResourceAsStream("Tracker/ui.fxml");
        if (res == null) {
            throw new AppError("Could not load resources");
        }
        try {
            box = loader.load(res);
        } catch (IOException err) {
            throw new AppError("Could not read resources: " + err.getMessage());
        }

        if (box == null) {
            throw new AppError("Could not build UI");
        }
        this.scene = new Scene(box);
    }

    public Scene getScene() {
        return this.scene;
    }
}
