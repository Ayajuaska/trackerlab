package Activities;

import javax.xml.bind.annotation.*;

public class baseActivity {
    // Затраты каллорий в час
    @XmlAttribute
    private float cost;

    @XmlAttribute
    private String accName;

    public baseActivity() {
        cost = 0;
        this.accName = this.getClass().getName();
    }
    baseActivity(float cost) {
        this.cost = cost;
        this.accName = this.getClass().getName();
    }

    public float cost(float hours) {
        return this.cost * hours;
    }

    public String toString() {
        return this.getClass().getName();
    }

    float getCost() {
        return cost;
    }

    void setCost(float cost) {
        this.cost = cost;
    }

    String getAccName() {
        return this.accName;
    }
}
