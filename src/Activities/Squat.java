package Activities;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "cost", "name"}, name = "PushUps")
@XmlElement
public class Squat extends baseActivity {
    private static float baseCost = 42;

    public Squat() {
        super(Squat.baseCost);
    }
}
