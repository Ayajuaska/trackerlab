package Activities;

import com.sun.xml.internal.txw2.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={ "cost", "accName"})
@XmlElement
public class PushUps extends baseActivity {
    private static float baseCost = 23;

    public PushUps() {
        super(PushUps.baseCost);
    }
}
