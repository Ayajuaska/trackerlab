package Events;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

public class ProfileEvent extends Event {
    private String profile;

    public static EventType<ProfileEvent> SELECTED = new EventType<>("SELECTED");
    public static EventType<ProfileEvent> TICK = new EventType<>("TICK");
    public ProfileEvent(EventType<? extends Event> eventType) {
        super(eventType);
    }

    public ProfileEvent(Object o, EventTarget eventTarget, EventType<? extends Event> eventType) {
        super(o, eventTarget, eventType);
    }

    public String getMsg() {
        return this.eventType.toString();
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getProfile() {
        return profile;
    }
}
