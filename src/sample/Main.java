package sample;
import Events.ProfileEvent;
import Profiles.Profile;
import Profiles.ProfileWindow;
import Tracker.TrackerWindow;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Main extends Application {

    private Stage stage;
    static public Properties properties;
    static private Logger logger = Logger.getLogger(Main.class.getName());
    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        ProfileWindow profileWindow = new ProfileWindow();
        Scene profilesScene = profileWindow.getScene();
        primaryStage.setScene(profilesScene);
        primaryStage.show();
        profilesScene.addEventHandler(ProfileEvent.SELECTED, this::profileSelected);
    }

    private void profileSelected(ProfileEvent event) {
        logger.log(Level.INFO, "Profile selected: " + event.getProfile());
        Profile.load(properties.getProperty("profiles.path") + File.separator + event.getProfile() + ".xml");
        stage.hide();
        Scene mainScene = new TrackerWindow().getScene();
        stage.setScene(mainScene);
        stage.show();
    }

    public static void main(String[] args) {
        properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("property.properties"));
        } catch (IOException err) {
            logger.log(Level.SEVERE, err.getMessage());
            return;
        }
        String path = System.getProperty("user.home") + File.separator +  properties.getProperty("profiles.path");
        properties.setProperty("profiles.path", path);
        File file = new File(path);

        if (file.mkdirs() || file.exists()) {
            launch(args);
        }
    }
}
