package Windows;

import javafx.scene.Scene;

public interface IWindow {
    Scene getScene();
}
